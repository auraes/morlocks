0 REM http://www.retroprogrammez.fr/aventure/morlocks/
0 REM http://www.retroprogrammez.fr/wp-content/uploads/2018/08/morlocks.txt

1 REM ===== LA CAVERNE DES MORLOCKS =====
2 REM Auteur  : FRANCOIS COULON   
3 REM L'OI HS 77 - Decembre 1985
4 REM Reprise : DOMINIQUE RIOUAL 
5 REM Version 1.1 - 28/07/2018 : correctif RND lignes 45 et 50
6 REM 
7 REM
8 REM
9 REM
10 TEXT : HOME
20 PRINT"        ";: INVERSE : PRINT" LA CAVERNE DES MORLOCKS ":NORMAL
30 PRINT : PRINT:PRINT"Par Francois Coulon,":PRINT "L'Ordinateur Individuel Hors Serie 77"
35 PRINT "Decembre 1985."
36 PRINT : PRINT "Adapte pour Apple II par D. Rioual,":PRINT "decembre 2017."
40 DIM A(6,10),A$(6,10) 
45 X1 = INT(RND(1)*6) + 1 : Y1 = INT(RND(1)*10) + 1
50 X3 = INT(RND(1)*6) + 1 : Y3 = INT(RND(1)*10) + 1 : IF X3 = 2 AND Y3 = 8 THEN 50
60 X = 1 : Y = 1
70 FOR X2 = 1 TO 6 : FOR Y2 = 1 TO 10 : READ A$(X2,Y2) : NEXT Y2 : NEXT X2
80 PRINT:PRINT:PRINT"Voulez-vous la regle du jeu ?"
90 GOSUB 4970 : IF NOT FL THEN 370
100 HOME : PRINT"--------------Regle du jeu-------------"
110 PRINT : PRINT
120 PRINT"18 avril 1934. Vous etes dans la jungle"
130 PRINT"Vous arrivez devant l'entree d'une"
140 PRINT"caverne."
150 PRINT"C'est le repaire des horribles Morlocks"
160 PRINT"et vous devez delivrer tous leurs"
170 PRINT"prisonniers. Vous decidez, a vos risques";
180 PRINT"et perils, d'y penetrer..."
190 PRINT
200 PRINT"A chaque fois, l'ordinateur vous decrit"
210 PRINT"la piece dans laquelle vous vous"
220 PRINT"trouvez et eventuellement vous pose une"
230 PRINT"question a laquelle vous repondez par"
240 PRINT"oui ou par non (O=oui N=non)."
250 PRINT
260 PRINT"Ensuite, il vous demande vers quelle"
270 PRINT"direction vous desirez aller."
280 PRINT"(O=Ouest, E=Est, N=Nord, S=Sud)"
290 PRINT : GET A$
300 PRINT"Tapez (I) pour un inventaire complet"
310 PRINT"des objets que vous avez pris."
320 PRINT"Ils peuvent vous etre utiles ou"
330 PRINT"nefastes."
340 PRINT
350 PRINT TAB(15);"Bonne chance !"
360 PRINT : GET A$
370 HOME : GOTO 550
380 XX = X : YY = Y
390 PRINT
400 PRINT"Quelle direction choisissez-vous ? ";
410 GET D$
420 IF D$ = "I" THEN PRINT D$:GOTO 5040
425 IF D$ = "Q" THEN 5500
430 IF D$ < > "N" AND D$ < > "S" AND D$ < > "E" AND D$ < > "O" AND D$ < > "Q" THEN 410
440 F1 = 1
450 FOR ZZ = 1 TO LEN(A$(X,Y)) : IF MID$(A$(X,Y),ZZ,1) = D$ THEN F1 = 0
460 NEXT ZZ
470 IF F1 THEN PRINT
480 IF F1 THEN PRINT "Impossible de prendre cette direction": X = XX : Y = YY : GOTO 400
490 IF D$ = "N" THEN D$ = "NORD" : Y = Y + 1
500 IF D$ = "S" THEN D$ = "SUD"  : Y = Y - 1
510 IF D$ = "O" THEN D$ = "OUEST": X = X - 1
520 IF D$ = "E" THEN D$ = "EST"  : X = X + 1
530 PRINT D$
540 PRINT : PRINT"________________________________________"
550 DD$=" "
560 FOR ZZ = 1 TO LEN(A$(X,Y)) : G$=MID$(A$(X,Y),ZZ,1) : GOSUB 5440 : DD$ = DD$ + G$ + " " : NEXT ZZ
570 PRINT"Directions possibles : ":PRINT:PRINT DD$ :PRINT
580 IF NOT(X=1 AND Y=1) THEN 660
590 PRINT"Une piece au sol couvert de sang. Trois"
600 PRINT"cadavres sont etales par terre."
610 IF CO THEN 660
620 PRINT"Un couteau est reste plante dans la"
630 PRINT"poitrine de l'un d'eux."
640 PRINT"Prenez-vous ce couteau ?"
650 GOSUB 4970:IF FL THEN CO=1
660 IF NOT(X=1 AND Y=2) THEN 710
670 PRINT"Un Morlock se dirige vers vous et"
680 PRINT"s'apprete a vous attaquer..."
690 IF CO THEN PRINT"Vous le tuez avec votre couteau":GOTO 710
700 PRINT"Il vous etrangle et c'est la fin...":GOTO 5210
710 IF NOT(X=1 AND Y=3) THEN 770
720 PRINT"Un squelette est assis dans un coin de"
730 PRINT"la piece."
740 PRINT"Lui adressez-vous la parole ?"
750 GOSUB 4970:IF FL THEN PRINT"Vous sentez que vous vous transformez":PRINT"en Morlock !!!":GOTO 5210
760 PRINT"Rien ne se passe..."
770 IF NOT(X=1 AND Y=4) THEN 860
780 PRINT"Une magnifique blonde se tient debout"
790 PRINT"devant vous. C'est une esclave."
800 IF BI THEN 860
810 PRINT"Elle vous dit 'Prenez ce bijou. Il vous"
820 PRINT"aidera.'"
830 PRINT"Prenez-vous ce bijou ?"
840 GOSUB 4970:IF FL THEN BI=1:PRINT"Elle vous regarde sans rien dire"
850 IF NOT BI THEN PRINT"Elle s'assied, un peu triste semble-t-il"
860 IF NOT(X=1 AND Y=5) THEN 910
870 PRINT"Une table. Dessus, un message qui vous"
880 PRINT"dit de ne pas craindre le bruit des"
890 PRINT"serpents et que le tresor doit etre"
900 PRINT"ouvert"
910 IF NOT(X=1 AND Y=6) THEN 970
920 PRINT"Quelques inscriptions sont peintes sur "
930 PRINT"les murs ainsi que des dessins qui"
940 PRINT"semblent prehistoriques."
950 PRINT"Vous approchez-vous de ces dessins ?"
960 GOSUB 4970: IF FL THEN PRINT"Rien de special, pourquoi ?"
970 IF NOT(X=1 AND Y=7) THEN 1040
980 PRINT"Des lutins dansent autour d'un puit..."
990 PRINT"Ils sont tres joyeux et vous invitent"
1000 PRINT"a regarder dedans."
1010 PRINT"Le faites-vous ?"
1020 GOSUB 4970 : IF FL THEN PRINT"Ils vous y precipitent allegrement...":GOTO 5210
1030 PRINT"Ils continuent leur danse..."
1040 IF NOT(X=1 AND Y=8) THEN 1110
1050 PRINT"Un vampire se tient devant vous. Il a la";
1060 PRINT"ferme intention de faire un bon repas..."
1070 IF AI THEN PRINT"Il remarque la gousse d'ail que vous":PRINT"tenez et en tombe raide mort":GOTO 1110
1080 PRINT"Dans l'impossibilite de vous defendre,"
1090 PRINT"il vous mord et suce votre sang."
1100 PRINT"C'est fini pour vous.":GOTO 5210
1110 IF NOT(X=1 AND Y=9) THEN 1210
1120 PRINT"Vous vous trouvez soudain face a face"
1130 PRINT"avec votre parfait sosie qui vous"
1140 PRINT"demande si vous allez bien."
1150 PRINT"Allez-vous bien ?"
1160 GOSUB 4970:IF FL THEN PRINT"Il vous repond 'Moi, ca va bien aussi'":GOTO 1210
1170 PRINT"Vous le voyez viellir a vu d'oeil !!!"
1180 PRINT"En regardant vos mains, vous comprenez"
1190 PRINT"que vous faites de meme. Dix secondes"
1200 PRINT"apres, vous n'etes plus qu'un squelette.":GOTO 5210
1210 IF NOT(X=1 AND Y=10) THEN 1300
1220 PRINT"Un globe de verre donne sur la jungle."
1230 IF NOT MR THEN 1300
1240 PRINT"Voulez-vous tenter de briser ce globe"
1250 PRINT"pour vous echapper ?"
1260 GOSUB 4970:IF NOT FL THEN 1300
1270 PRINT"Dans cette jungle regne une atmosphere"
1280 PRINT"irrespirable et vous mourez donc par asphyxie."
1290 GOTO 5210
1300 IF NOT(X=2 AND Y=1) THEN 1360
1310 PRINT"Une voix vous dit 'Appuyez sur le bouton";
1320 PRINT"vert pour connaitre la suite'"
1330 PRINT"Tapez-vous sur ce bouton ?"
1340 GOSUB 4970 : IF FL THEN PRINT"Elle repond 'Cela vient de vous sauver":PRINT"la vie'"
1350 IF NOT(FL) THEN PRINT"L'eau envahit la piece et vous mourez":PRINT"noye.": GOTO 5210
1360 IF NOT(X=2 AND Y=2) THEN 1410
1370 PRINT"Vous etes dans un long couloir. Sur les"
1380 PRINT"murs sont accrochees des torches."
1390 PRINT"En prenez-vous une ?"
1400 GOSUB 4970 : IF FL THEN TR=1
1410 IF NOT(X=2 AND Y=3) THEN 1450
1420 IF NOT TR THEN PRINT"Une grotte obscure. Impossible de":PRINT"trouver la sortie. Vous restez bloque":PRINT"la, a tout jamais":GOTO 5210
1430 PRINT"Une piece eclairee seulement par la"
1440 PRINT"lumiere de votre torche"
1450 IF NOT(X=2 AND Y=4) THEN 1550
1460 PRINT"Une machine etrange... Dessus,il y a un"
1470 PRINT"levier rouge"
1480 PRINT"Tentez-vous d'utiliser cette machine ?"
1490 GOSUB 4970:IF NOT FL THEN 1550
1500 PRINT"Elle permet de disparaitre et de"
1510 PRINT"reapparaitre dans une autre piece au"
1520 PRINT"hasard..."
1530 X = INT(RND(1)*6+1) : Y = INT(RND(1)*10+1)
1540 GOTO 540
1550 IF NOT(X=2 AND Y=5) THEN 1620
1560 PRINT"Vous etes dans une piece ou se trouve"
1570 PRINT"une enclume et des outils"
1580 IF MR THEN 1620
1590 PRINT"Vous remarquez un marteau a rayures."
1600 PRINT"Prenez-vous ce marteau ?"
1610 GOSUB 4970: IF FL THEN MR=1
1620 IF NOT(X=2 AND Y=6) THEN 1690
1630 PRINT"Une grande serre ou poussent des fleurs"
1640 PRINT"inconnues. Vous entendez des pas..."
1650 PRINT"Restez-vous ici ?"
1660 GOSUB 4970:IF NOT FL THEN 1690
1670 PRINT"La trappe d'une oubliette s'ouvre sous"
1680 PRINT"vos pieds.": GOTO 5210
1690 IF NOT(X=2 AND Y=7) THEN 1740
1700 PRINT"Vous etes dans une piece ou se trouve"
1710 PRINT"une trappe."
1720 PRINT"Tentez-vous de l'ouvrir ?"
1730 GOSUB 4970:IF FL THEN PRINT"Ouelques rats en sortent..."
1740 IF NOT(X=2 AND Y=8) THEN 1790
1750 PRINT"Vous remarquez un journal datant de"
1760 PRINT"1935 ainsi qu'un passage secret."
1770 PRINT"L'empruntez-vous ?"
1780 GOSUB 4970 : IF FL THEN X = X3 : Y = Y3 : GOTO 540
1790 IF NOT(X=2 AND Y=9) THEN 1850
1800 PRINT"Deux Morlocks vont se jeter sur vous !"
1810 IF TR THEN PRINT"Mais ils ont trop peur du feu de votre torche":GOTO 1850
1820 IF CO THEN PRINT"Malgre votre couteau, ils reussissent a vous tuer":GOTO 5210
1830 IF MR THEN PRINT"Vous tuez le premier avec votre marteau mais le second est trop fort pour vous ":GOTO 5210
1840 PRINT"Sans armes, vous ne pouviez rien faire. ":PRINT"Vous etes mort.":GOTO 5210
1850 IF NOT(X=2 AND Y=10) THEN 1890
1860 PRINT"Un robot vous declare 'AIAHABAYAOADATAY'";
1870 PRINT"Bien sur,sans la cle du code, impossible";
1880 PRINT"de dechiffrer ce message..."
1890 IF NOT(X=3 AND Y=1) THEN 1980
1900 PRINT"Vous etes dans une piece ou trois loups"
1910 PRINT"sont attaches."
1920 IF TR THEN PRINT"Par bonheur, ils ont peur de votre torche":GOTO 1980
1930 PRINT"Vous vous apercevez a vos depens que"
1940 PRINT"la corde qui les retient est assez"
1950 PRINT"longue pour qu'ils vous atteignent et"
1960 PRINT"vous devorent."
1970 GOTO 5210
1980 IF NOT(X=3 AND Y=2) THEN 2010
1990 PRINT"Vous etes dans une piece vide."
2000 IF TR THEN PRINT"Mais vous glissez et laisser tomber votre torche dans l'eau":TR=0
2010 IF NOT(X=3 AND Y=3) THEN 2110
2020 PRINT"Un buffet Henri II"
2030 IF AI THEN 2070
2040 PRINT"Une gousse d'ail y est posee."
2050 PRINT"Prenez-vous cette gousse d'ail ?"
2060 GOSUB 4970 : IF FL THEN AI=1
2070 PRINT"Il y a aussi un livre intitule :"
2080 PRINT"'La cuisine de A a Z'"
2090 PRINT"Prenez-vous ce livre ?"
2100 GOSUB 4970 : IF FL THEN AZ=1
2110 IF NOT(X=3 AND Y=4) THEN 2220
2120 PRINT"Vous etes dans une piece sombre ou se"
2130 PRINT"trouve un coffre ferme par un lourd"
2140 PRINT"cadenas."
2150 PRINT"Voulez-vous l'ouvrir ?"
2160 GOSUB 4970 : IF NOT FL THEN 2220
2170 IF NOT NR THEN PRINT"Impossible de l'ouvrir...":GOTO 2220
2180 IF MR THEN PRINT"Victoire ! Le coffre est ouvert mais"
2190 PRINT"des Morlocks surpris par le bruit font"
2200 PRINT"irruption dans la piece et vous"
2210 PRINT"tranchent la gorge.":GOTO 5210
2220 IF NOT(X=3 AND Y=5) THEN 2300
2230 PRINT"Une fontaine representant un lion"
2240 IF NOT BI THEN FL=0:GOTO 2290
2250 PRINT"Vous remarquez un petit trou ou"
2260 PRINT"logerait juste votre bijou."
2270 PRINT"Mettez-vous votre bijou dans ce trou ?"
2280 GOSUB 4970 : IF FL THEN PRINT"La fontaine s'ouvre et laisse apparaitre un message disant 'Vous avez de la chance, reprenez votre bijou'"
2290 IF NOT FL THEN PRINT"Vous recevez un jet d'eau bouillante.":PRINT"Vous etes mort.":GOTO 5210
2300 IF NOT(X=3 AND Y=6) THEN 2360
2310 PRINT"Vous etes dans une piece au sol plein"
2320 PRINT"d'huile. Il regne une forte odeur de":PRINT"gaz."
2330 PRINT"Vous attardez-vous dans cette piece ?"
2340 GOSUB 4970 : IF FL AND TR THEN PRINT"Il se produit alors une forte explosion (pensez a votre torche). Vous etes mort.":GOTO 5210
2350 PRINT"Rien de special...."
2360 IF NOT(X=3 AND Y=7) THEN 2460
2370 PRINT"Un cuisinier Morlock vous regarde"
2380 PRINT"etrangement..."
2390 IF NOT AZ THEN 2440
2400 PRINT"Il vous arrache votre livre des mains"
2410 PRINT"et regarde a la page de l'homme aux"
2420 PRINT"carottes. Inutile de dire que vous allez"
2430 PRINT"mourir." : GOTO 5210
2440 PRINT"Il y a une lueur de deception dans ses"
2450 PRINT"gros yeux noirs..."
2460 IF NOT(X=3 AND Y=8) THEN 2500
2470 PRINT"Une voix vous dit 'Vous avez devant vous";
2480 PRINT"une potion. En buvez-vous ?'"
2490 GOSUB 4970 : IF FL THEN PRINT"Rien pour l'instant..." : PO = 1
2500 IF NOT(X=3 AND Y=9) THEN 2550
2510 PRINT"Une longue piece ou sont entasses de"
2520 PRINT"nombreux squelettes. Ca sent tres"
2530 PRINT"mauvais."
2540 PRINT"A part cela, il y a aussi quelques":PRINT"araignees."
2550 IF NOT(X=3 AND Y=10) THEN 2620
2560 PRINT"Un long couloir... Un homme vous demande";
2570 PRINT"Avez-vous le message ?"
2580 IF NOT ME THEN PRINT"Comme vous ne l'avez pas, il appelle les gardes qui vous tuent":GOTO 5210
2590 PRINT"Heureusement, vous l'avez. Vous lui"
2600 PRINT"montrez et il vous murmure a l'oreille"
2610 PRINT"'C'est dur la vie d'artiste'"
2620 IF NOT(X=4 AND Y=1) THEN 2690
2630 PRINT"Vous etes dans une bibliotheque aux"
2640 PRINT"livres pleins de poussiere..."
2650 IF ME THEN 2690
2660 PRINT"Vous remarquez un message"
2670 PRINT"incomprehensible. Le prenez-vous ?"
2680 GOSUB 4970:IF FL THEN ME=1
2690 IF NOT(X=4 AND Y=2) THEN 2740
2700 PRINT"Une piece pleine d'esclaves enchaines..."
2710 PRINT"Ils vous supplient de les delivrer."
2720 PRINT"Essayez-vous de le faire ?"
2730 GOSUB 4970:IF FL THEN PRINT"Et comment ca ?"
2740 IF NOT(X=4 AND Y=3) THEN 2790
2750 PRINT"Une petite salle vide. Dans un coin de"
2760 PRINT"la piece est pose un petit paquet."
2770 PRINT"Desirez-vous l'ouvrir ?"
2780 GOSUB 4970:IF FL THEN PRINT"Malheureusement, il est piege, vous etesmort.":GOTO 5210
2790 IF NOT(X=4 AND Y=4) THEN 2830
2800 PRINT"Vous etes a l'interieur d'une petite"
2810 PRINT"chapelle faiblement eclairee. Sur le"
2820 PRINT"sol, quelques rats morts..."
2830 IF NOT(X=4 AND Y=5) THEN 2950
2840 PRINT"Vous etes devant un enorme tronc"
2850 PRINT"d'arbre. En lui donnant un coup de pied,";
2860 PRINT"vous vous rendez compte qu'il sonne"
2870 PRINT"creux."
2880 PRINT"L'examinez-vous de plus pres ?"
2890 GOSUB 4970 : IF NOT FL THEN 2950
2900 PRINT"Il y a les restes d'une ruche"
2910 IF MI THEN 2950
2920 PRINT"avec encore un peu de miel."
2930 PRINT"Le prenez-vous ?"
2940 GOSUB 4970 : IF FL THEN MI = 1
2950 IF NOT(X=4 AND Y=6) THEN 3040
2960 PRINT"Une piece vide. Soudain, vous sentez un"
2970 PRINT"couteau sur votre gorge."
2980 PRINT"On vous dit de donner tout votre or."
2990 IF NOT GO THEN PRINT"Vous n'en avez pas, votre agresseur vous":PRINT"tue.":GOTO 5210
3000 PRINT"Vous le faites..."
3010 PRINT"Votre attaquant vous dit que vous avez"
3020 PRINT"de la chance. Il s'en va en courant."
3030 GO=0
3040 IF NOT(X=4 AND Y=7) THEN 3100
3050 PRINT"Un enorme ours est pret a vous attaquer.";
3060 IF NOT MI THEN PRINT"Il vous saute dessus et vous mange.":GOTO 5210
3070 PRINT"Heureusement pour vous, vous avez du":PRINT"miel."
3080 PRINT"Il le mange et vous laisse tranquille."
3090 MI=0
3100 IF NOT(X=4 AND Y=8) THEN 3160
3110 PRINT"Une petite salle avec un coffre."
3120 IF GO THEN 3160
3130 PRINT"Il y a dedans de l'or."
3140 PRINT"Le prenez-vous ?"
3150 GOSUB 4970 : IF FL THEN GO=1
3160 IF NOT(X=4 AND Y=9) THEN 3230
3170 PRINT"Une piece vide a part un Apple II sur"
3180 PRINT"une table situee au milieu de la salle."
3190 PRINT"Sur son ecran est ecrit :"
3200 D = ABS(X-X1) + ABS(Y-Y1)
3210 PRINT"'Encore ";D;" piece(s) et vous gagnez.'"
3220 PRINT"Il y a aussi quelques pommes..."
3230 IF NOT(X=4 AND Y=10) THEN 3290
3240 PRINT"Une piece immense..."
3250 PRINT"Sur un autel de sacrifice, la statuette"
3260 PRINT"d'un Morlock."
3270 PRINT"L'emportez-vous ?"
3280 GOSUB 4970 : IF FL THEN ST=1
3290 IF NOT(X=5 AND Y=1) THEN 3380
3300 PRINT"Un couloir boueux, eclaire seulement par";
3310 PRINT"le jour que fait apparaitre une"
3320 PRINT"cheminee communiquant avec l'air libre."
3330 PRINT"Regardez-vous le jour par la cheminee ?"
3340 GOSUB 4970 :IF NOT FL THEN 3380
3350 PRINT"Des Morlocks vous jettent des pierres"
3360 PRINT"de la surface..."
3370 GOTO 5210
3380 IF NOT(X=5 AND Y=2) THEN 3480
3390 PRINT"Une grande salle de banquet (bizarre"
3400 PRINT"chez les Morlocks)."
3410 PRINT"Il y a une bonne odeur de cuisine."
3420 PRINT"Vous vous sentez fatigue..."
3430 PRINT"Vous reposez-vous ?"
3440 GOSUB 4970 : IF NOT(FL AND TR) THEN 3480
3450 PRINT"Avec votre torche, vous enflammez la"
3460 PRINT"piece. Vous perissez dans un magnifique"
3470 PRINT"incendie." : GOTO 5210
3480 IF NOT(X=5 AND Y=3) THEN 3550
3490 PRINT"Une piece pleine de coquillages et de"
3500 PRINT"sable..."
3510 PRINT"Prenez-vous un peu de sable ?"
3520 GOSUB 4970 : IF FL THEN SB=1
3530 PRINT"Prenez-vous un de ces coquillages ?"
3540 GOSUB 4970 : IF FL THEN CQ=1
3550 IF NOT(X=5 AND Y=4) THEN 3640
3560 PRINT"Vous etes devant une grande horloge qui"
3570 PRINT"semble marcher encore car un tic-tac se"
3580 PRINT"fait toujours entendre ainsi que le"
3590 PRINT"balancier qui bouge toujours."
3600 PRINT"Restez-vous pour observer ?"
3610 GOSUB 4970 : IF NOT FL THEN 3640
3620 PRINT"Vous vous apercevez que les aiguilles"
3630 PRINT"tournent a l'envers !!"
3640 IF NOT(X=5 AND Y=5) THEN 3720
3650 PRINT"Un salon ou sont installes, en rond, des"
3660 PRINT"fauteuils.(bizarre chez les Morlocks)"
3670 PRINT"Sur la table centrale, une feuille de"
3680 PRINT"papier sur laquelle est ecrit"
3690 PRINT"'Clef du code secret'"
3700 PRINT"Prenez-vous cette feuille ?"
3710 GOSUB 4970 : IF FL THEN CS=1
3720 IF NOT(X=5 AND Y=6) THEN 3810
3730 PRINT"Une piece vide sauf quelques cafards..."
3740 IF NOT CS THEN 3810
3750 PRINT"Vous entendez crier a cote 'La cle du"
3760 PRINT"code secret a disparu !!!'"
3770 PRINT"Un homme vous surprend, il dit que vous"
3780 PRINT"etes un voleur. Il vous tue et reprend"
3790 PRINT"la clef du code."
3800 GOTO 5210
3810 IF NOT(X=5 AND Y=7) THEN 3900
3820 PRINT"Un homme visiblement chinois vous dit :"
3830 PRINT"'La logique est l'art de deceler"
3840 PRINT"l'evidence cachee'. Etes-vous d'accord ?"
3850 GOSUB 4970 : IF FL THEN 3890
3860 PRINT"Il dit 'Soyez maudit !' Des dizaines de"
3870 PRINT"Morlocks surgissent dans la piece et"
3880 PRINT"vous tuent." : GOTO 5210
3890 PRINT"Il dit 'Vous etes tres sage'"
3900 IF NOT(X=5 AND Y=8) THEN 3960
3910 PRINT"Un phonographe...":IF DI THEN 3960
3920 PRINT"Il y a aussi le premier disque de Tino"
3930 PRINT"Rossi."
3940 PRINT"Prenez-vous ce disque ?"
3950 GOSUB 4970 : IF FL THEN DI = 1
3960 IF NOT(X=5 AND Y=9) THEN 3990
3970 PRINT"Rien, sauf des chaines, certainement"
3980 PRINT"pour les futurs esclaves."
3990 IF NOT(X=5 AND Y=10) THEN 4100
4000 PRINT"Un lac souterrain inquietant, l'eau"
4010 PRINT"semble tres sale. Il y a aussi un"
4020 PRINT"revolver sur l'autre rive. Il est"
4030 PRINT"possible d'aller le chercher en"
4040 PRINT"contournant le lac."
4050 PRINT"Le faites-vous ?"
4060 GOSUB 4970 : IF NOT FL THEN 4100
4070 PRINT"Vous glissez sur une pierre et vous"
4080 PRINT"tombez dans l'eau. Elle est si froide"
4090 PRINT"que vous etes gele en trois secondes.": GOTO 5210
4100 IF NOT(X=6 AND Y=1) THEN 4190
4110 PRINT"Derriere vous, vous entendez le bruit"
4120 PRINT"d'un serpent. Vous n'avez pas le temps"
4130 PRINT"de vous sauver. Il vous mord..."
4140 IF TR AND CO THEN 4160
4150 PRINT"Vous mourez en quelques minutes.":GOTO 5210
4160 PRINT"Vous cauterisez la plaie avec votre"
4170 PRINT"torche et votre couteau. Ca fait mal"
4180 PRINT"mais vous survivez."
4190 IF NOT (X=6 AND Y=2) THEN 4210
4200 PRINT"Des habits de moines sur des cintres."
4210 IF NOT(X=6 AND Y=3) THEN 4300
4220 PRINT"Une assemblee de moines.Ils portent tous";
4230 PRINT"des torches a la main. Ils sont assis."
4240 IF TR THEN 4280
4250 PRINT"L'un d'eux s'approche de vous. Il a une"
4260 PRINT"face de squelette. Il vous enfonce un"
4270 PRINT"poignard dans les cotes.":GOTO 5210
4280 PRINT"Ils s'ecrient tous ensembles 'Soyez des"
4290 PRINT"notres !!'"
4300 IF NOT(X=6 AND Y=4) THEN 4400
4310 PRINT"Une vieille manette toute rouillee. Vous";
4320 PRINT"pouvez la tirer vers vous."
4330 PRINT"Le faites-vous ?"
4340 GOSUB 4970 : IF NOT FL THEN 4400
4350 PRINT"Une partie du sol s'ouvre sous vos"
4360 PRINT"pieds. Vous tombez dans une fosse pleine";
4370 PRINT"de serpents et d'araignees. Impossible de"
4380 PRINT"de remonter."
4390 GOTO 5210
4400 IF NOT(X=6 AND Y=5) THEN 4460
4410 PRINT"Une salle pleine de pierres."
4420 IF NOT DI THEN 4460
4430 PRINT"Soudain, vous trebuchez et vous faites"
4440 PRINT"tomber votre disque de Tino Rossi, il se":PRINT"casse."
4450 DI=0
4460 IF NOT(X=6 AND Y=6) THEN 4500
4470 PRINT"Une piece vide aux murs gris et sales."
4480 PRINT"Il y regne une odeur particulierement"
4490 PRINT"nauseabonde vous faisant penser a un":PRINT"egout."
4500 IF NOT(X=6 AND Y=7) THEN 4620
4510 PRINT"Un laboratoire avec de nombreuses"
4520 PRINT"eprouvettes, des tubes en verre et de"
4530 PRINT"multiples bocaux remplis d'etranges":PRINT"liquides..."
4540 PRINT"Un homme en blouse blanche s'approche"
4550 PRINT"et vous dit ' Avez-vous mon disque":PRINT"prefere ?'"
4560 IF NOT DI THEN PRINT"Vous ne l'avez pas...":GOTO 4600
4570 PRINT"Il voit le disque de Tino Rossi et"
4580 PRINT"tente de s'en emparer. Le laissez-vous faire ?"
4590 GOSUB 4970 : IF FL THEN DI=0:GOTO 4620
4600 PRINT"Il repand dans la piece une sorte de":PRINT"gaz."
4610 PO=1
4620 IF NOT(X=6 AND Y=8) THEN 4660
4630 PRINT"Une grande piece pleine de poussiere."
4640 PRINT"Il y a quelques corps defigures et"
4650 PRINT"plein de sang..."
4660 IF NOT(X=6 AND Y=9) THEN 4750
4670 IF TR THEN 4710
4680 PRINT"Une piece totalement obscure. Vous"
4690 PRINT"n'avez pas le temps de voir le Morlock"
4700 PRINT"qui se jette sur vous et vous tue.":GOTO 5210
4710 PRINT"Une piece sombre, eclairee par votre"
4720 PRINT"torche. Vous avez juste le temps de voir";
4730 PRINT"le Morlock qui veut vous attaquer et de"
4740 PRINT"fuir. Il ne vous rejoint pas."
4750 IF NOT(X=6 AND Y=10) THEN 4770
4760 PRINT"Une piece vide. Rien ne se passe."
4770 IF X1=X AND Y1=Y THEN 4790
4780 GOTO 380
4790 F$="***************************************"
4800 PRINT F$:PRINT "*          Vous avez gagne !!         *":PRINT F$
4810 PRINT"En regardant la piece dans laquelle"
4820 PRINT"vous etes, vous avez remarque un"
4830 PRINT"message contenant la formule magique"
4840 PRINT"permettant de mettre les Morlocks ainsi"
4850 PRINT"que toute personne malfaisante de la"
4860 PRINT"grotte hors d'etat de nuire !"
4870 PRINT"Vous remarquez aussi la clef permettant"
4880 PRINT"d'ouvrir toutes les chaines !"
4890 IF NOT BI THEN 4920
4900 PRINT"En allant delivrer la blonde, vous serez";
4910 PRINT"sur d'avoir l'amour pour la vie !"
4920 IF NOT PO THEN 4950
4930 PRINT:PRINT"Mais une etrange maladie vous emporte"
4940 PRINT"en quelques semaines...":GOTO 5210
4950 PRINT:PRINT"Un passage secret mene vers l'exterieur"
4960 PRINT:INVERSE:PRINT"BRAVO !!!!":NORMAL:END
4970 REM - Saisie choix
4980 GET A$ : IF A$ <> "N" AND A$ <> "O" AND A$ <> "Q" THEN 4980
4985 IF A$ = "Q" THEN 5500
4990 IF A$ = "O" THEN FL = -1 : A$= "OUI"
5000 REM NOT(0)=-1 NOT(-1)=0
5010 IF A$ = "N" THEN FL = 0 : A$ = "NON"
5020 PRINT"->"; A$
5030 RETURN
5040 PRINT:PRINT"--------INVENTAIRE DES OBJETS--------"
5050 PRINT
5060 IF CO THEN PRINT"Un couteau"
5070 IF BI THEN PRINT"Un bijou"
5080 IF TR THEN PRINT"Une torche"
5090 IF MR THEN PRINT"Un marteau"
5100 IF AI THEN PRINT"Une gousse d'ail"
5110 IF AZ THEN PRINT"Un livre 'La cuisine de A a Z '"
5120 IF ME THEN PRINT"Un message code"
5130 IF MI THEN PRINT"Du miel"
5140 IF GO THEN PRINT"De l'or"
5150 IF ST THEN PRINT"La statuette d'un Morlock"
5160 IF SB THEN PRINT"Un peu de sable"
5170 IF CQ THEN PRINT"Un coquillage"
5180 IF CS THEN PRINT"La clef du code secret"
5190 IF DI THEN PRINT"Un disque de Tino Rossi"
5200 GOTO 390
5210 PRINT:PRINT:PRINT TAB(9);:INVERSE:PRINT "*** VOUS ETES MORT ***":NORMAL
5220 PRINT: PRINT"Voulez-vous jouer encore ?"
5230 GOSUB 4970 : IF FL THEN CLEAR : GOTO 10
5240 PRINT:PRINT TAB(15);"Au revoir !"
5250 END
5260 DATA NE,NS,NSE,NSE
5270 DATA NS,NSE,NSE,NSE
5280 DATA NSE,S
5290 DATA EO,NE,EOS,O
5300 DATA E,NO,SEO,NEO
5310 DATA NSEO,S
5320 DATA NO,NSEO,SEO,NE
5330 DATA NSO,SE,NO,NSO
5340 DATA NSEO,S
5350 DATA E,EO,NEO,OS
5360 DATA NE,NSEO,NSO,SE
5370 DATA NEO,SE
5380 DATA NEO,NSEO,SEO,NE
5390 DATA NSEO,NSO,NS,NSEO
5400 DATA NSEO,SEO
5410 DATA NO,SO,NO,NSO
5420 DATA NSO,NS,NS,SO
5430 DATA NO,SO
5440 IF G$ = "O" THEN G$ = "OUEST"
5450 IF G$ = "E" THEN G$ = "EST"
5460 IF G$ = "S" THEN G$ = "SUD"
5470 IF G$ = "N" THEN G$ = "NORD"
5480 RETURN
5499 REM----- FIN DE LA PARTIE
5500 PRINT : PRINT "VOUS QUITTEZ LE JEU... FIN DE LA PARTIE"
5510 END

